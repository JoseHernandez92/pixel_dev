const sectionTemplate = document.createElement('template')
sectionTemplate.innerHTML = `
  <section>
    <div class='container'>
      <h3>Sobre Mi...</h3>
    </div>
  </section>
`

class Section extends HTMLElement{
  constructor(){
    super()
    this.root = this.attachShadow({ mode: 'open'})
    this.root.appendChild(sectionTemplate.content.cloneNode(true))
  }

  static get observedAttributes() {
    return ['label']
  }

  attributeChangedCallback(name, _oldVal, newVal) {
    this.labelChange(name, newVal)
  }

  labelChange(attributeName, attributeContent) {
    if(attributeName == 'label'){
    }
  }
}

window.customElements.define('pixel-section', Section)
