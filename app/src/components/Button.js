import { NewBus } from '../NewBus'

const buttonTemplate = document.createElement('template')
buttonTemplate.innerHTML = `
  <a class='navItem'></a>
`

class Button extends HTMLElement{
  constructor(){
    super()
    this.bus = NewBus
    this.root = this.attachShadow({ mode: 'open'})
    this.root.appendChild(buttonTemplate.content.cloneNode(true))
    this.link = this.root.querySelector('a')
    this.link.onclick = ()=>{
      const eventName = this.link.getAttribute('href')
      this.sendEvent(eventName)
      return false
    }
  }

  static get observedAttributes() {
    return ['label']
  }

  attributeChangedCallback(name, _oldVal, newVal) {
    this.labelChange(name, newVal)
  }

  labelChange(attributeName, attributeContent) {
    if(attributeName == 'label'){
      this.link.setAttribute ('label', attributeContent)
      this.link.setAttribute ('id', attributeContent)
      this.link.setAttribute('href', attributeContent)
      this.link.innerHTML = attributeContent
    }
  }

  sendEvent(eventName){
    console.log (eventName)
    this.bus.publish('navButtonClick', eventName)
  }

}

window.customElements.define('pixel-button', Button)
