# pixel_dev
# online developer curriculum

## Development

Requisites: `docker` & `docker-compose`

### Up & running
Run: `docker-compose up --build`. 
APP is accesible via browser [localhost:8080](http://localhost:8080) and API is listening on [localhost:8081](http://localhost:8081)

## Frontend
Deploy to heroku: https://pixel-dev-app.herokuapp.com/
## Backend
Deploy to heroku: https://pixel-dev-api.herokuapp.com/